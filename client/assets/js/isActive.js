let params = new URLSearchParams(window.location.search)
let courseId = params.get("courseId")
let isActive = document.querySelector("#isActiveTrue")
let token = localStorage.getItem("token")

    isActive.addEventListener("click", () => {

    fetch(`http://localhost:3000/api/courses/${courseId}`, {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        },      
    })
    .then(res => {
        return res.json()
    })
    .then(data => {
        if(data === true){
            //if creation of new course is successful, redirect to courses page
            window.location.replace('./courses.html')
        }
    })
})
